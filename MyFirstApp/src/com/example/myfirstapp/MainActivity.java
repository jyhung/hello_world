package com.example.myfirstapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    public void clearBoard (View view) {
    	turn = "X";
    	TextView tv = (TextView)findViewById(R.id.button1);
    	tv.setText(" ");
    	tv = (TextView)findViewById(R.id.button2);
    	tv.setText(" ");
    	tv = (TextView)findViewById(R.id.button3);
    	tv.setText(" ");
    	tv = (TextView)findViewById(R.id.button4);
    	tv.setText(" ");
    	tv = (TextView)findViewById(R.id.button5);
    	tv.setText(" ");
    	tv = (TextView)findViewById(R.id.button6);
    	tv.setText(" ");
    	tv = (TextView)findViewById(R.id.button7);
    	tv.setText(" ");
    	tv = (TextView)findViewById(R.id.button8);
    	tv.setText(" ");
    	tv = (TextView)findViewById(R.id.button9);
    	tv.setText(" ");
    }
    String turn = "X";
    // I'm sorry, I really could not figure out how to pass in the button ID
    // so I had to copy/paste... I looked into getTag for a while, but couldn't
    // figure it out.
    public void ticTacToeClick1 (View view) {
    	TextView tv = (TextView)findViewById(R.id.button1);
    	tv.setText(turn);
    	tv = (TextView)findViewById(R.id.turnView);
    	if(turn == "X") {
    		turn = "O";
    		tv.setText("Turn O");
    	} else {
    		turn = "X";
    		tv.setText("Turn X");
    	}
    }
    
    public void ticTacToeClick2 (View view) {
    	TextView tv = (TextView)findViewById(R.id.button2);
    	tv.setText(turn);
    	tv = (TextView)findViewById(R.id.turnView);
    	if(turn == "X") {
    		turn = "O";
    		tv.setText("Turn O");
    	} else {
    		turn = "X";
    		tv.setText("Turn X");
    	}
    }
    
    public void ticTacToeClick3 (View view) {
    	TextView tv = (TextView)findViewById(R.id.button3);
    	tv.setText(turn);
    	tv = (TextView)findViewById(R.id.turnView);
    	if(turn == "X") {
    		turn = "O";
    		tv.setText("Turn O");
    	} else {
    		turn = "X";
    		tv.setText("Turn X");
    	}
    }
 
    public void ticTacToeClick4 (View view) {
    	TextView tv = (TextView)findViewById(R.id.button4);
    	tv.setText(turn);
    	tv = (TextView)findViewById(R.id.turnView);
    	if(turn == "X") {
    		turn = "O";
    		tv.setText("Turn O");
    	} else {
    		turn = "X";
    		tv.setText("Turn X");
    	}
    }
    
    public void ticTacToeClick5 (View view) {
    	TextView tv = (TextView)findViewById(R.id.button5);
    	tv.setText(turn);
    	tv = (TextView)findViewById(R.id.turnView);
    	if(turn == "X") {
    		turn = "O";
    		tv.setText("Turn O");
    	} else {
    		turn = "X";
    		tv.setText("Turn X");
    	}
    }
    
    public void ticTacToeClick6 (View view) {
    	TextView tv = (TextView)findViewById(R.id.button6);
    	tv.setText(turn);
    	tv = (TextView)findViewById(R.id.turnView);
    	if(turn == "X") {
    		turn = "O";
    		tv.setText("Turn O");
    	} else {
    		turn = "X";
    		tv.setText("Turn X");
    	}
    }
    
    public void ticTacToeClick7 (View view) {
    	TextView tv = (TextView)findViewById(R.id.button7);
    	tv.setText(turn);
    	tv = (TextView)findViewById(R.id.turnView);
    	if(turn == "X") {
    		turn = "O";
    		tv.setText("Turn O");
    	} else {
    		turn = "X";
    		tv.setText("Turn X");
    	}
    }
    
    public void ticTacToeClick8 (View view) {
    	TextView tv = (TextView)findViewById(R.id.button8);
    	tv.setText(turn);
    	tv = (TextView)findViewById(R.id.turnView);
    	if(turn == "X") {
    		turn = "O";
    		tv.setText("Turn O");
    	} else {
    		turn = "X";
    		tv.setText("Turn X");
    	}
    }
    
    public void ticTacToeClick9 (View view) {
    	TextView tv = (TextView)findViewById(R.id.button9);
    	tv.setText(turn);
    	tv = (TextView)findViewById(R.id.turnView);
    	if(turn == "X") {
    		turn = "O";
    		tv.setText("Turn O");
    	} else {
    		turn = "X";
    		tv.setText("Turn X");
    	}
    }
}
